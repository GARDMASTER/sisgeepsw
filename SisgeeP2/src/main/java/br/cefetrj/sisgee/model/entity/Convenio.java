package br.cefetrj.sisgee.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Classe da entidade Convenio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Convenio {

	@Id
	@GeneratedValue
	private Long idConvenio;

	private String numeroConvenio;
	private Date dataInicioConvenio;
	private Date dataFimConvenio;

	@ManyToOne
	private Empresa empresa;

	@OneToMany(mappedBy = "convenio")
	private List<TermoEstagio> termosEstagios;

	public Convenio() {
		super();
	}

	public Convenio(String numeroConvenio, Date dataInicioConvenio, Date dataFimConvenio, Empresa empresa) {
		super();
		this.numeroConvenio = numeroConvenio;
		this.dataInicioConvenio = dataInicioConvenio;
		this.dataFimConvenio = dataFimConvenio;
		this.empresa = empresa;
	}

	
	
	public Long getIdConvenio() {
		return idConvenio;
	}

	public String getNumeroConvenio() {
		return numeroConvenio;
	}

	public Date getDataInicioConvenio() {
		return dataInicioConvenio;
	}

	public Date getDataFimConvenio() {
		return dataFimConvenio;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public List<TermoEstagio> getTermosEstagios() {
		return termosEstagios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idConvenio ^ (idConvenio >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Convenio other = (Convenio) obj;
		if (idConvenio != other.idConvenio)
			return false;
		return true;
	}

	public String toString() {
		return numeroConvenio;
	}
}
