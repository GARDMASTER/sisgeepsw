package br.cefetrj.sisgee.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Classe da entidade TermoAditivo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class TermoAditivo {

	@Id
	@GeneratedValue
	private Long idTermoAditivo;

	private Date dataFimTermoAditivo;
	private short cargaHorariaTermoAditivo;
	private float valorBolsaTermoAditivo;
	private String enderecoTermoAditivo;
	private String numeroEnderecoTermoAditivo;
	private String complementoEnderecoTermoEstagio;
	private String bairroEnderecoTermoAditivo;
	private String cepEnderecoTermoAditivo;
	private String cidadeEnderecoTermoAditivo;
	private String estadoEnderecoTermoAditivo;

	@ManyToOne
	private TermoEstagio termoEstagio;

	public TermoAditivo() {
		
	}

	public TermoAditivo(Date dataFimTermoAditivo, short cargaHorariaTermoAditivo, float valorBolsaTermoAditivo,
			String enderecoTermoAditivo, String numeroEnderecoTermoAditivo, String complementoEnderecoTermoEstagio,
			String bairroEnderecoTermoAditivo, String cepEnderecoTermoAditivo, String cidadeEnderecoTermoAditivo,
			String estadoEnderecoTermoAditivo, TermoEstagio termoEstagio) {
		super();
		this.dataFimTermoAditivo = dataFimTermoAditivo;
		this.cargaHorariaTermoAditivo = cargaHorariaTermoAditivo;
		this.valorBolsaTermoAditivo = valorBolsaTermoAditivo;
		this.enderecoTermoAditivo = enderecoTermoAditivo;
		this.numeroEnderecoTermoAditivo = numeroEnderecoTermoAditivo;
		this.complementoEnderecoTermoEstagio = complementoEnderecoTermoEstagio;
		this.bairroEnderecoTermoAditivo = bairroEnderecoTermoAditivo;
		this.cepEnderecoTermoAditivo = cepEnderecoTermoAditivo;
		this.cidadeEnderecoTermoAditivo = cidadeEnderecoTermoAditivo;
		this.estadoEnderecoTermoAditivo = estadoEnderecoTermoAditivo;
		this.termoEstagio = termoEstagio;
	}

	
	
	public Long getIdTermoAditivo() {
		return idTermoAditivo;
	}

	public Date getDataFimTermoAditivo() {
		return dataFimTermoAditivo;
	}

	public void setDataFimTermoAditivo(Date dataFimTermoAditivo) {
		this.dataFimTermoAditivo = dataFimTermoAditivo;
	}

	public short getCargaHorariaTermoAditivo() {
		return cargaHorariaTermoAditivo;
	}

	public void setCargaHorariaTermoAditivo(short cargaHorariaTermoAditivo) {
		this.cargaHorariaTermoAditivo = cargaHorariaTermoAditivo;
	}

	public float getValorBolsaTermoAditivo() {
		return valorBolsaTermoAditivo;
	}

	public void setValorBolsaTermoAditivo(float valorBolsaTermoAditivo) {
		this.valorBolsaTermoAditivo = valorBolsaTermoAditivo;
	}

	public String getEnderecoTermoAditivo() {
		return enderecoTermoAditivo;
	}

	public void setEnderecoTermoAditivo(String enderecoTermoAditivo) {
		this.enderecoTermoAditivo = enderecoTermoAditivo;
	}

	public String getNumeroEnderecoTermoAditivo() {
		return numeroEnderecoTermoAditivo;
	}

	public void setNumeroEnderecoTermoAditivo(String numeroEnderecoTermoAditivo) {
		this.numeroEnderecoTermoAditivo = numeroEnderecoTermoAditivo;
	}

	public String getComplementoEnderecoTermoEstagio() {
		return complementoEnderecoTermoEstagio;
	}

	public void setComplementoEnderecoTermoEstagio(String complementoEnderecoTermoEstagio) {
		this.complementoEnderecoTermoEstagio = complementoEnderecoTermoEstagio;
	}

	public String getBairroEnderecoTermoAditivo() {
		return bairroEnderecoTermoAditivo;
	}

	public void setBairroEnderecoTermoAditivo(String bairroEnderecoTermoAditivo) {
		this.bairroEnderecoTermoAditivo = bairroEnderecoTermoAditivo;
	}

	public String getCepEnderecoTermoAditivo() {
		return cepEnderecoTermoAditivo;
	}

	public void setCepEnderecoTermoAditivo(String cepEnderecoTermoAditivo) {
		this.cepEnderecoTermoAditivo = cepEnderecoTermoAditivo;
	}

	public String getCidadeEnderecoTermoAditivo() {
		return cidadeEnderecoTermoAditivo;
	}

	public void setCidadeEnderecoTermoAditivo(String cidadeEnderecoTermoAditivo) {
		this.cidadeEnderecoTermoAditivo = cidadeEnderecoTermoAditivo;
	}

	public String getEstadoEnderecoTermoAditivo() {
		return estadoEnderecoTermoAditivo;
	}

	public void setEstadoEnderecoTermoAditivo(String estadoEnderecoTermoAditivo) {
		this.estadoEnderecoTermoAditivo = estadoEnderecoTermoAditivo;
	}

	public TermoEstagio getTermoEstagio() {
		return termoEstagio;
	}

	public void setTermoEstagio(TermoEstagio termoEstagio) {
		this.termoEstagio = termoEstagio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idTermoAditivo ^ (idTermoAditivo >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermoAditivo other = (TermoAditivo) obj;
		if (idTermoAditivo != other.idTermoAditivo)
			return false;
		return true;
	}

}
