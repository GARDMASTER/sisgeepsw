package br.cefetrj.sisgee.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.TermoAditivo;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade TermoAditivo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */
public class AditivoDAO extends GenericDAO{
	
	/**
	 * Construtor padr�o de AditivoDAO
	 * 
	 */
	public AditivoDAO() {
		super(TermoAditivo.class, PersistenceManager.getEntityManager());
	}
	
	/**
	 * Busca TermoAditivo por Aluno
	 * 
	 * @return lista de TermoAditivo
	 */
	public List buscarAditivosPorAluno(Aluno aluno){
		EntityManager em = PersistenceManager.getEntityManager();
		Query query = em.createQuery("select a from TermoAditivo a where a.aluno = :aluno", TermoAditivo.class);
		query.setParameter("aluno", aluno);
		return query.getResultList();
	}
	
	
	
}
