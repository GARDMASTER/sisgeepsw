package br.cefetrj.sisgee.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Classe Respons�vel pelo acesso ao dado de Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */
public class EmpresaDAO extends GenericDAO{

	/**
	 * Construtor padr�o de EmpresaDAO
	 * 
	 */
	public EmpresaDAO(){
		super(Empresa.class, PersistenceManager.getEntityManager());
	}
	
	
	/**
	 * Busca Empresa por CNPJ
	 * 
	 * @return lista de Empresa
	 */
	public Empresa buscarEmpresaPorString(String cnpj){
		EntityManager em = PersistenceManager.getEntityManager();
		Query query = em.createQuery("select a from Empresa a where a.cnpjEmpresa = :cnpjEmpresa", Empresa.class);
		query.setParameter("cnpjEmpresa", cnpj);
		return (Empresa) query.getSingleResult();
	}
	
	
	public Empresa buscarEmpresaPorAgente(String cnpj, Long idAgente){
		EntityManager em = PersistenceManager.getEntityManager();
		System.out.println(cnpj + " "+ idAgente);
		Query query = em.createQuery("select a from Empresa a where a.cnpjEmpresa = :cnpjEmpresa and a.agenteintegracao_idagenteintegracao = :idagente", Empresa.class);
		query.setParameter("cnpjEmpresa", cnpj);
		query.setParameter("idagente", idAgente);
		
		try {
			Empresa empresa =(Empresa) query.getSingleResult(); 
			return empresa;
		} catch (Exception e) {
			Empresa empresa =null;
			return empresa;
		}
		
	}

}

