package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Classe da entidade Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Aluno {

	@Id
	@GeneratedValue
	private Long idAluno;

	private String matricula;
	private String situacao;

	@ManyToOne
	private Pessoa pessoa;

	@ManyToOne
	private Curso curso;

	@OneToMany(mappedBy = "aluno")
	private List<TermoEstagio> termosEstagios;

	public Aluno() {

	}

	public Aluno(String matricula, String situacao, Pessoa pessoa, Curso curso) {
		super();
		this.matricula = matricula;
		this.situacao = situacao;
		this.pessoa = pessoa;
		this.curso = curso;
	}

	
	
	public Long getIdAluno() {
		return idAluno;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<TermoEstagio> getTermosEstagios() {
		return termosEstagios;
	}

	public void setTermosEstagios(List<TermoEstagio> termosEstagios) {
		this.termosEstagios = termosEstagios;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idAluno ^ (idAluno >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (idAluno != other.idAluno)
			return false;
		return true;
	}

	public String toString() {
		return matricula;
	}

}
