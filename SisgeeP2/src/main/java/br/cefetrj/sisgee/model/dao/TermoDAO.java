package br.cefetrj.sisgee.model.dao;

import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Empresa;
import br.cefetrj.sisgee.model.entity.TermoEstagio;

@Transactional
public class TermoDAO extends GenericDAO{

	/**
	 * Construtor padr�o de EmpresaDAO
	 * 
	 */
	public TermoDAO(){
		super(TermoEstagio.class, PersistenceManager.getEntityManager());
	}
	
	
	/**
	 * Inclui no Termo de Estagio uma data de rescis�o
	 * 
	 * @return TermoEstagio
	 */
	public boolean incluirDataRescisao(Date dataRescisao, Long idaluno){
		EntityManager em = PersistenceManager.getEntityManager();
		System.out.println(dataRescisao + " " + idaluno);
			Query query = em.createQuery("UPDATE TermoEstagio SET datarescisaotermoestagio = :dataRescisao where aluno_idaluno = :idaluno");
			query.setParameter("dataRescisao", dataRescisao);
			query.setParameter("idaluno", idaluno);
			em.joinTransaction();
			query.executeUpdate();
			return true;
			
		
	}

	
	public static TermoEstagio buscarTermoEstagioPorAluno(Long idaluno){
		EntityManager em = PersistenceManager.getEntityManager();
		try {
			Query query = em.createQuery("select a from TermoEstagio a where a.idaluno = :idaluno", TermoEstagio.class);
			query.setParameter("idaluno", idaluno);
			return (TermoEstagio) query.getSingleResult();
		} catch (Exception e) {
			TermoEstagio termoEstagio = null;
			return termoEstagio;
		}
	}
	
	

}
