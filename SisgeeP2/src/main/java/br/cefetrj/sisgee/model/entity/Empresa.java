package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Classe da entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Empresa {

	@Id
	@GeneratedValue
	private Long idEmpresa;

	private String cnpjEmpresa;
	private String nomeEmpresa;

	@ManyToOne
	private AgenteIntegracao agenteIntegracao;

	@OneToMany(mappedBy = "empresa")
	private List<Convenio> convenios;

	public Empresa() {
		super();
	}

	public Empresa(String cnpjEmpresa, String nomeEmpresa) {
		super();
		this.cnpjEmpresa = cnpjEmpresa;
		this.nomeEmpresa = nomeEmpresa;
	}

	public Empresa(String cnpjEmpresa, String nomeEmpresa, AgenteIntegracao agenteIntegracao) {
		super();
		this.cnpjEmpresa = cnpjEmpresa;
		this.nomeEmpresa = nomeEmpresa;
		this.agenteIntegracao = agenteIntegracao;
	}

	
	
	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public String getCnpjEmpresa() {
		return cnpjEmpresa;
	}

	public void setCnpjEmpresa(String cnpjEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public AgenteIntegracao getAgenteIntegracao() {
		return agenteIntegracao;
	}

	public void setAgenteIntegracao(AgenteIntegracao agenteIntegracao) {
		this.agenteIntegracao = agenteIntegracao;
	}

	public List<Convenio> getConvenios() {
		return convenios;
	}

	public void setConvenios(List<Convenio> convenios) {
		this.convenios = convenios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idEmpresa ^ (idEmpresa >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (idEmpresa != other.idEmpresa)
			return false;
		return true;
	}

	public String toString() {
		return nomeEmpresa;
	}

}
