package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Classe da entidade AgenteIntegracao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */
@Entity
public class AgenteIntegracao {

	@Id
	@GeneratedValue
	private Long idAgenteIntegracao;

	private String cnpjAgenteIntegracao;
	private String nomeAgenteIntegracao;

	@OneToMany(mappedBy = "agenteIntegracao")
	private List<Empresa> empresas;

	public AgenteIntegracao() {
		super();
	}

	public AgenteIntegracao(String cnpjAgenteIntegracao, String nomeAgenteIntegracao) {
		super();
		this.cnpjAgenteIntegracao = cnpjAgenteIntegracao;
		this.nomeAgenteIntegracao = nomeAgenteIntegracao;
	}
	
	

	public Long getIdAgenteIntegracao() {
		return idAgenteIntegracao;
	}

	public String getCnpjAgenteIntegracao() {
		return cnpjAgenteIntegracao;
	}

	public void setCnpjAgenteIntegracao(String cnpjAgenteIntegracao) {
		this.cnpjAgenteIntegracao = cnpjAgenteIntegracao;
	}

	public String getNomeAgenteIntegracao() {
		return nomeAgenteIntegracao;
	}

	public void setNomeAgenteIntegracao(String nomeAgenteIntegracao) {
		this.nomeAgenteIntegracao = nomeAgenteIntegracao;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public String toString() {
		return nomeAgenteIntegracao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idAgenteIntegracao ^ (idAgenteIntegracao >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgenteIntegracao other = (AgenteIntegracao) obj;
		if (idAgenteIntegracao != other.idAgenteIntegracao)
			return false;
		return true;
	}

}
