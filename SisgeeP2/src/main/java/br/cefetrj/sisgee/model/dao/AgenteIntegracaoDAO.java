package br.cefetrj.sisgee.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.cefetrj.sisgee.model.entity.AgenteIntegracao;

public class AgenteIntegracaoDAO extends GenericDAO<AgenteIntegracao>{

	/**
	 * Construtor padr�o de AgenteIntegracaoDAO
	 * 
	 */
	public AgenteIntegracaoDAO() {
		super(AgenteIntegracao.class, PersistenceManager.getEntityManager());
	}
	
	/**
	 * Busca AgenteIntegracao por CNPJ
	 * 
	 * @return objeto AgenteIntegracao
	 */
	public AgenteIntegracao buscarAgentePorString(String cnpj){
		EntityManager em = PersistenceManager.getEntityManager();
		Query query = em.createQuery("select a from AgenteIntegracao a where a.cnpjAgenteIntegracao = :cnpjAgenteIntegracao", AgenteIntegracao.class);
		query.setParameter("cnpjAgenteIntegracao", cnpj);
		return (AgenteIntegracao) query.getSingleResult();
	}

}
