package br.cefetrj.sisgee.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Classe da entidade Pessoa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Pessoa {

	@Id
	@GeneratedValue
	private Long idPessoa;

	private String cpf;
	private String nome;
	private Date dataNascimento;
	private String tipoEndereco;
	private String endereco;
	private String numeroEndereco;
	private String complementoEndereco;
	private String bairroEndereco;
	private String cepEndereco;
	private String cidadeEndereco;
	private String estadoEndereco;
	private String paisEndereco;
	private String email;
	private int telefoneResidencial01;
	private int telefoneResidencial02;
	private int telefoneComercial01;
	private int telefoneComercial02;
	private int telefoneCelular01;
	private int telefoneCelular02;
	
	@OneToMany(mappedBy="pessoa")
	private List<Aluno> alunos;

	public Pessoa() {

	}

	public Pessoa(String cpf, String nome, Date dataNascimento, String tipoEndereco, String endereco,
			String numeroEndereco, String complementoEndereco, String bairroEndereco, String cepEndereco,
			String cidadeEndereco, String estadoEndereco, String paisEndereco, String email, int telefoneResidencial01,
			int telefoneResidencial02, int telefoneComercial01, int telefoneComercial02, int telefoneCelular01,
			int telefoneCelular02) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.tipoEndereco = tipoEndereco;
		this.endereco = endereco;
		this.numeroEndereco = numeroEndereco;
		this.complementoEndereco = complementoEndereco;
		this.bairroEndereco = bairroEndereco;
		this.cepEndereco = cepEndereco;
		this.cidadeEndereco = cidadeEndereco;
		this.estadoEndereco = estadoEndereco;
		this.paisEndereco = paisEndereco;
		this.email = email;
		this.telefoneResidencial01 = telefoneResidencial01;
		this.telefoneResidencial02 = telefoneResidencial02;
		this.telefoneComercial01 = telefoneComercial01;
		this.telefoneComercial02 = telefoneComercial02;
		this.telefoneCelular01 = telefoneCelular01;
		this.telefoneCelular02 = telefoneCelular02;
	}

	
	
	public Long getIdPessoa() {
		return idPessoa;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(String tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}

	public String getComplementoEndereco() {
		return complementoEndereco;
	}

	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco;
	}

	public String getBairroEndereco() {
		return bairroEndereco;
	}

	public void setBairroEndereco(String bairroEndereco) {
		this.bairroEndereco = bairroEndereco;
	}

	public String getCepEndereco() {
		return cepEndereco;
	}

	public void setCepEndereco(String cepEndereco) {
		this.cepEndereco = cepEndereco;
	}

	public String getCidadeEndereco() {
		return cidadeEndereco;
	}

	public void setCidadeEndereco(String cidadeEndereco) {
		this.cidadeEndereco = cidadeEndereco;
	}

	public String getEstadoEndereco() {
		return estadoEndereco;
	}

	public void setEstadoEndereco(String estadoEndereco) {
		this.estadoEndereco = estadoEndereco;
	}

	public String getPaisEndereco() {
		return paisEndereco;
	}

	public void setPaisEndereco(String paisEndereco) {
		this.paisEndereco = paisEndereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTelefoneResidencial01() {
		return telefoneResidencial01;
	}

	public void setTelefoneResidencial01(int telefoneResidencial01) {
		this.telefoneResidencial01 = telefoneResidencial01;
	}

	public int getTelefoneResidencial02() {
		return telefoneResidencial02;
	}

	public void setTelefoneResidencial02(int telefoneResidencial02) {
		this.telefoneResidencial02 = telefoneResidencial02;
	}

	public int getTelefoneComercial01() {
		return telefoneComercial01;
	}

	public void setTelefoneComercial01(int telefoneComercial01) {
		this.telefoneComercial01 = telefoneComercial01;
	}

	public int getTelefoneComercial02() {
		return telefoneComercial02;
	}

	public void setTelefoneComercial02(int telefoneComercial02) {
		this.telefoneComercial02 = telefoneComercial02;
	}

	public int getTelefoneCelular01() {
		return telefoneCelular01;
	}

	public void setTelefoneCelular01(int telefoneCelular01) {
		this.telefoneCelular01 = telefoneCelular01;
	}

	public int getTelefoneCelular02() {
		return telefoneCelular02;
	}

	public void setTelefoneCelular02(int telefoneCelular02) {
		this.telefoneCelular02 = telefoneCelular02;
	}

	
	
	
	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idPessoa ^ (idPessoa >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (idPessoa != other.idPessoa)
			return false;
		return true;
	}
	
	public String toString() {
		return nome;
	}

}
