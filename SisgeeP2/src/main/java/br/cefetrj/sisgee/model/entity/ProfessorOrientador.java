package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Classe da entidade ProfessorOrientador
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class ProfessorOrientador {

	@Id
	@GeneratedValue
	private Long idProfessorOrientador;

	private String nomeProfessorOrientador;

	@OneToMany(mappedBy = "professorOrientador")
	private List<TermoEstagio> termosEstagios;

	public ProfessorOrientador() {
		super();
	}

	public ProfessorOrientador(String nomeProfessorOrientador) {
		super();
		this.nomeProfessorOrientador = nomeProfessorOrientador;
	}

	
	
	public Long getIdProfessorOrientador() {
		return idProfessorOrientador;
	}

	public void setIdProfessorOrientador(Long idProfessorOrientador) {
		this.idProfessorOrientador = idProfessorOrientador;
	}

	public String getNomeProfessorOrientador() {
		return nomeProfessorOrientador;
	}

	public void setNomeProfessorOrientador(String nomeProfessorOrientador) {
		this.nomeProfessorOrientador = nomeProfessorOrientador;
	}

	public List<TermoEstagio> getTermosEstagios() {
		return termosEstagios;
	}

	public void setTermosEstagios(List<TermoEstagio> termosEstagios) {
		this.termosEstagios = termosEstagios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idProfessorOrientador ^ (idProfessorOrientador >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorOrientador other = (ProfessorOrientador) obj;
		if (idProfessorOrientador != other.idProfessorOrientador)
			return false;
		return true;
	}

	public String toString() {
		return nomeProfessorOrientador;
	}

}
