package br.cefetrj.sisgee.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Convenio;

public class ConvenioDAO extends GenericDAO{
	
	public ConvenioDAO(){
		super(Convenio.class, PersistenceManager.getEntityManager());
	}
	
	public Convenio buscarConvenioPorString(String numeroConvenio){
		EntityManager em = PersistenceManager.getEntityManager();
		Query query = em.createQuery("select a from Convenio a where a.numeroConvenio = :numeroConvenio", Convenio.class);
		query.setParameter("numeroConvenio", numeroConvenio);
		try {
			return (Convenio) query.getSingleResult();
		} catch (Exception e) {
			Convenio convenio = null;
			return convenio;
		}
		
	}
	
}
