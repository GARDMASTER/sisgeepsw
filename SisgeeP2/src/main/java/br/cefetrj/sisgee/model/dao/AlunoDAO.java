package br.cefetrj.sisgee.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Campus;
import br.cefetrj.sisgee.model.entity.Curso;

/**
 * Classe Respons�vel pelo acesso ao dado da entidade Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */
public class AlunoDAO extends GenericDAO{
	/**
	 * Construtor padr�o de AlunoDAO
	 * 
	 */
	public AlunoDAO(){
		super(Aluno.class, PersistenceManager.getEntityManager());
	}
	
	/**
	 * Busca Aluno por Matricula
	 * 
	 * @return lista de Aluno
	 */
	public Aluno buscarAlunoPorString(String matricula){
		EntityManager em = PersistenceManager.getEntityManager();
		Query query = em.createQuery("select a from Aluno a where a.matricula = :matricula", Aluno.class);
		query.setParameter("matricula", matricula);
		return (Aluno) query.getSingleResult();
	}

}
