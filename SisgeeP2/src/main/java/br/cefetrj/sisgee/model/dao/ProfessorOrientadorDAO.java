package br.cefetrj.sisgee.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.cefetrj.sisgee.model.entity.ProfessorOrientador;

public class ProfessorOrientadorDAO extends GenericDAO<ProfessorOrientador>{

	public ProfessorOrientadorDAO( ) {
		super(ProfessorOrientador.class, PersistenceManager.getEntityManager());	
	}

	public ProfessorOrientador buscarProfessorOrientadorPorId(Long id) {
		EntityManager em = PersistenceManager.getEntityManager();
		Query query = em.createQuery("select a from ProfessorOrientador a where a.id = :id", ProfessorOrientador.class);
		query.setParameter("id", id);
		return (ProfessorOrientador) query.getSingleResult();
	}
}
