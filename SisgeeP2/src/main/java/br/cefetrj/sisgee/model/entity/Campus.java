package br.cefetrj.sisgee.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Classe da entidade Campus
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

@Entity
public class Campus {

	@Id
	@GeneratedValue
	private Long idCampus;

	private String nomeCampus;

	@OneToMany(mappedBy = "campus")
	private List<Curso> cursos;

	public Campus() {

	}

	public Campus(String nomeCampus) {
		super();
		this.nomeCampus = nomeCampus;
	}

	
	
	public Long getIdCampus() {
		return idCampus;
	}

	public String getNomeCampus() {
		return nomeCampus;
	}

	public void setNomeCampus(String nomeCampus) {
		this.nomeCampus = nomeCampus;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idCampus ^ (idCampus >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Campus other = (Campus) obj;
		if (idCampus != other.idCampus)
			return false;
		return true;
	}

	public String toString() {
		return nomeCampus;
	}
	
}
