package br.cefetrj.sisgee.view.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.entity.AgenteIntegracao;
import br.cefetrj.sisgee.model.entity.ProfessorOrientador;

/**
 * Tag customizada da entidade AgenteIntegracao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class AgenteIntegracaoTag extends SimpleTagSupport{
private long id;
	
	@Override
	public void doTag() throws JspException, IOException {
		String retorno = ""
				
				
				+ "<select id='agenteIntegracao' class='form-control' name='agenteIntegracao'>"
				+ "	<option value=''>...</option>";
		
		GenericDAO<AgenteIntegracao> agenteIntegracaoDAO = PersistenceManager.createGenericDAO(AgenteIntegracao.class);
		List<AgenteIntegracao> listaAI = agenteIntegracaoDAO.buscarTodos();
		
		for (AgenteIntegracao agenteIntegracao : listaAI) {
			retorno += "<option value='" + agenteIntegracao.getIdAgenteIntegracao() +"' ";
			if(agenteIntegracao.getIdAgenteIntegracao().equals(id)){
				retorno += " selected ";
			}
			retorno += " >" + agenteIntegracao.getNomeAgenteIntegracao() + "</option>";
		}
		retorno += "</select>";
		
		getJspContext().getOut().append(retorno);
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
