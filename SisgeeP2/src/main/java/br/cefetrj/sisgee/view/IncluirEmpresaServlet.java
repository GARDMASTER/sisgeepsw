package br.cefetrj.sisgee.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.EmpresaServices;
import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Servlet para incluir a entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class IncluirEmpresaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cnpjEmpresa = request.getParameter("cnpj");
		String nomeEmpresa = request.getParameter("razaoSocial");
		String agenteIntegracao = request.getParameter("agenteIntegracao");	
		
		if (request.getParameter("radio").equals("S")) {
			request.getRequestDispatcher("/IncluirAgenteServlet").forward(request, response);
		}else {		
			Empresa empresa = new Empresa();
			empresa.setCnpjEmpresa(cnpjEmpresa);
			empresa.setNomeEmpresa(nomeEmpresa);
			empresa.setAgenteIntegracao(null);
			empresa.setConvenios(null);
			EmpresaServices.incluirEmpresa(empresa);
			
			request.getRequestDispatcher("form.jsp").forward(request, response);
		}
	}

}
