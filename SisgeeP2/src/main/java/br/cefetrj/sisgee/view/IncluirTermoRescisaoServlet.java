package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.TermoServices;
import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.TermoEstagio;

/**
 * Servlet implementation class IncluirTermoRescisaoServlet
 */
public class IncluirTermoRescisaoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String dataR = request.getParameter("dataRescisao");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			Date dataRescisao = null;
			try {
				dataRescisao = new Date(format.parse(dataR).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		Aluno aluno = BuscarAlunoServlet.getAluno();
		Long idaluno = aluno.getIdAluno();
		TermoServices.incluirDataRescisao(dataRescisao, idaluno);
		
		
		request.getRequestDispatcher("/sucesso.jsp").forward(request, response);
		
	}

}
