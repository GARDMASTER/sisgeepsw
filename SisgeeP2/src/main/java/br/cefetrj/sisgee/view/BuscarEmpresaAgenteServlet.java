package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.EmpresaServices;
import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Convenio;
import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Servlet implementation class BuscarEmpresaAgenteServlet
 */
public class BuscarEmpresaAgenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Empresa empresaAgente;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String agenteIntegracao = request.getParameter("agenteIntegracao");
		String empresaAgenteCnpj = request.getParameter("empresaAgenteCnpj");
		Long idAgente = null;
		String msg = "";
		
		try {
			idAgente = Long.parseLong(agenteIntegracao);
		} catch (Exception e) {
			msg += "Agente integra��o inexistente";
		}
		
		Empresa empresaAgente = EmpresaServices.buscarEmpresaPorAgente(empresaAgenteCnpj, idAgente);
		List<Empresa> dadosEmpresaAgente = new ArrayList<Empresa>();
		if (empresaAgente!=null) {
			dadosEmpresaAgente.add(empresaAgente);
		}
		
		
		
		Aluno aluno = BuscarAlunoServlet.getAluno();
		List<Aluno> dadosAluno = new ArrayList<Aluno>();
		if (aluno!=null) {
			dadosAluno.add(aluno);
		}
		
		Empresa empresa = BuscarEmpresaServlet.getEmpresa();
		List<Empresa> dadosEmpresa = new ArrayList<Empresa>();
		if (empresa!=null) {
			dadosEmpresa.add(empresa);
		}
		
		
		Convenio convenio = BuscarEmpresaServlet.getConvenio();
		List<Convenio> dadosConvenio = new ArrayList<Convenio>();
		dadosConvenio.add(convenio);
	
		if (empresaAgente == null) {
			msg += "N�o existe empresa ligada a esse agente. Cadastre uma nova empresa ligada ao Agente";
			
		}
		
		setEmpresaAgente(empresaAgente);
		request.setAttribute("dadosConvenio", dadosConvenio);
		request.setAttribute("dadosEmpresaAgente", dadosEmpresaAgente);
		request.setAttribute("dadosEmpresa", dadosEmpresa);
		request.setAttribute("dadosAluno", dadosAluno);
		
		if(msg.equals("")){
			request.getRequestDispatcher("/form.jsp").forward(request, response);
		
		}else{
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/form.jsp").forward(request, response);
		}
		
	}

	public static Empresa getEmpresaAgente() {
		return empresaAgente;
	}

	public static void setEmpresaAgente(Empresa empresaAgente) {
		BuscarEmpresaAgenteServlet.empresaAgente = empresaAgente;
	}

	
}
