package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.model.entity.AgenteIntegracao;
import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Servlet implementation class BuscarAgenteServlet
 */
public class BuscarAgenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static AgenteIntegracao agenteIntegracao;
 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cnpj = request.getParameter("cnpjAgente");
		
		Aluno aluno = BuscarAlunoServlet.getAluno();
		List<Aluno> dadosAluno = new ArrayList<Aluno>();
		if (aluno!=null) {
			dadosAluno.add(aluno);
		}
		
		
	}

	public static AgenteIntegracao getAgenteIntegracao() {
		return agenteIntegracao;
	}

	public static void setAgenteIntegracao(AgenteIntegracao agenteIntegracao) {
		BuscarAgenteServlet.agenteIntegracao = agenteIntegracao;
	}
}
