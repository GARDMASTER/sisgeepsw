package br.cefetrj.sisgee.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet para validar a entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class ValidaEmpresaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cnpjEmpresa = request.getParameter("cnpj");
		String nomeEmpresa = request.getParameter("razaoSocial");
		String radio = request.getParameter("radio");
		String msg = "";
		
		Long cnpjNum;
		try {
			cnpjNum = Long.parseLong(cnpjEmpresa);
		} catch (Exception e) {
			msg += "Cnpj inv�lido. " + "<br/>";
		}
		
		if (cnpjEmpresa.trim() != null && cnpjEmpresa.length() != 14 ) {
			msg += "O Cnpj deve ter 14 caracteres. " + "<br/>";
		}
		
		if (cnpjEmpresa.trim().length() == 0) {
			msg += "O Cnpj � um campo obrigat�rio. " + "<br/>";
		}
		
		if (nomeEmpresa.trim().length() == 0) {
			msg += "A Raz�o Social � um campo obrigat�rio. " + "<br/>";
		}
		
		if (nomeEmpresa.trim() != null && nomeEmpresa.length() > 100) {
			msg += "O nome da empresa deve ter at� 100 caracteres. " + "<br/>";
		}
		
		if(radio == null) {
			msg += "O campo Agente Integra��o � um campo obrigat�rio. " + "<br/>";
		}
		
		if(msg.equals("")){
			request.getRequestDispatcher("/IncluiEmpresaServlet").forward(request, response);
		
		}else{
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/registroEmpresa.jsp").forward(request, response);
		}
	}

}
