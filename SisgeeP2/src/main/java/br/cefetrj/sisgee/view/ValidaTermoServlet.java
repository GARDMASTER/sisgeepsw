package br.cefetrj.sisgee.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet para validar a entidade TermoEstagio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class ValidaTermoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String numConvenio = request.getParameter("numConvenio");
		String radioAgente = request.getParameter("radioAgente");
		String cnpj = request.getParameter("cnpj");
		String razaoSocial = request.getParameter("razaoSocial");
		String matricula = request.getParameter("matricula");
		String nome = request.getParameter("nome");
		String curso = request.getParameter("curso");
		String unidade = request.getParameter("unidade");
		String dataInicio = request.getParameter("dataInicio");
		String dataTermino = request.getParameter("dataTermino");
		String cargaHoraria = request.getParameter("cargaHoraria");
		String valorBolsa = request.getParameter("valorBolsa");
		String enderecoEstagio = request.getParameter("enderecoEstagio");
		String complementoEndereco = request.getParameter("complementoEndereco");
		String bairro = request.getParameter("bairro");
		String cidade = request.getParameter("cidade");
		String estado = request.getParameter("estado");
		String cep = request.getParameter("cep");
		String radioEstagio = request.getParameter("radioEstagio");
		String professorOrientador = request.getParameter("professorOrientador");
		String msg = "";
		
		/*if (numConvenio != null && numConvenio.toString().length() != 10 ) {
			msg += "O n�mero do conv�nio deve ter 10 caracteres. " + "<br/>";
		}
		
		if (numConvenio.toString().trim().length() == 0) {
			msg += "O n�mero do conv�nio � um campo obrigat�rio. " + "<br/>";
		}/*
		
		if (cnpj.trim() != null && cnpj.length() != 14 ) {
			msg += "O Cnpj deve ter 14 caracteres. " + "<br/>";
		}
		
		if (cnpj.trim().length() == 0) {
			msg += "O Cnpj � um campo obrigat�rio. " + "<br/>";
		}
		
		if (matricula.trim() != null && matricula.length() > 100 ) {
			msg += "A matricula deve possuir at� 100 caracteres. " + "<br/>";
		}
		
		if (matricula.trim().length() == 0) {
			msg += "A matricula � um campo obrigat�rio. " + "<br/>";
		}
		
		if (nome.trim() != null && nome.length() > 100 ) {
			msg += "O nome deve possuir at� 100 caracteres. " + "<br/>";
		}
		
		if (nome.trim().length() == 0) {
			msg += "O nome � um campo obrigat�rio. " + "<br/>";
		}
		
		if (curso.trim() != null && curso.length() > 255 ) {
			msg += "O curso deve possuir at� 255 caracteres. " + "<br/>";
		}
		
		if (nome.trim().length() == 0) {
			msg += "O curso � um campo obrigat�rio. " + "<br/>";
		}
		
		if (unidade.trim() != null && unidade.length() > 100 ) {
			msg += "A unidade deve possuir at� 100 caracteres. " + "<br/>";
		}
		
		if (unidade.trim().length() == 0) {
			msg += "A unidade � um campo obrigat�rio. " + "<br/>";
		}
		
		if (dataInicio.trim().length() == 0) {
			msg += "A Data de Inicio � um campo obrigat�rio. " + "<br/>";
		}
		
		
		if (cargaHoraria.trim() != null && cargaHoraria.length() != 1 ) {
			msg += "A carga de horas diaria deve ter 1 caracter. " + "<br/>";
		}
		
		if (cargaHoraria.trim().length() == 0) {
			msg += "A carga de horas diaria � um campo obrigat�rio. " + "<br/>";
		}
		
		if (valorBolsa.trim() != null && valorBolsa.length() > 10 ) {
			msg += "O valor da bolsa deve possuir at� 10 caracteres. " + "<br/>";
		}
		
		if (valorBolsa.trim().length() == 0) {
			msg += "O valor da bolsa � um campo obrigat�rio. " + "<br/>";
		}
		
		if (enderecoEstagio.trim() != null && enderecoEstagio.length() > 255 ) {
			msg += "O endereco deve possuir at� 255 caracteres. " + "<br/>";
		}
		
		if (enderecoEstagio.trim().length() == 0) {
			msg += "O endereco � um campo obrigat�rio. " + "<br/>";
		}
		
		if (complementoEndereco.trim() != null && complementoEndereco.length() > 150 ) {
			msg += "O complemento deve possuir at� 150 caracteres. " + "<br/>";
		}
		
		if (complementoEndereco.trim().length() == 0) {
			msg += "O complemento � um campo obrigat�rio. " + "<br/>";
		}
		
		if (bairro.trim() != null && bairro.length() > 150 ) {
			msg += "O bairro deve possuir at� 100 caracteres. " + "<br/>";
		}
		
		if (bairro.trim().length() == 0) {
			msg += "O bairro � um campo obrigat�rio. " + "<br/>";
		}
		
		if (cidade.trim() != null && cidade.length() > 150 ) {
			msg += "A cidade deve possuir at� 100 caracteres. " + "<br/>";
		}
		
		if (cidade.trim().length() == 0) {
			msg += "A cidade � um campo obrigat�rio. " + "<br/>";
		}
		
		if (cep.trim() != null && cep.length() != 15 ) {
			msg += "O cep deve possuir 15 caracteres. " + "<br/>";
		}
		
		if (cidade.trim().length() == 0) {
			msg += "O cep � um campo obrigat�rio. " + "<br/>";
		}
		
		if(radioEstagio == null) {
			msg += "O campo Agente Integra��o � um campo obrigat�rio. " + "<br/>";
		}
		
		if(estado == null) {
			msg += "O estado � um campo obrigat�rio. " + "<br/>";
		}
		
		if(radioEstagio.equals("S")) {
			if(professorOrientador.equals("")) {
				msg += "O professor orientador � um campo obrigat�rio quando o est�gio � obrigatorio. " + "<br/>";
			}
		}*/
		
		if(msg.equals("")){
			request.getRequestDispatcher("/IncluirTermoServlet").forward(request, response);
		
		}else{
			System.out.println(msg);
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("/form.jsp").forward(request, response);
		}
	}

}
