package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.AditivoServices;
import br.cefetrj.sisgee.model.entity.Aluno;

/**
 * Servlet para buscar a entidade TermoAditivo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class BuscarTermoAditivoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Aluno aluno = BuscarAlunoServlet.getAluno();
		List<Aluno> dadosAluno = new ArrayList<Aluno>();
		if (aluno!=null) {
			dadosAluno.add(aluno);
		}
		
		/*List termoAditivo = AditivoServices.buscarAditivoPorAluno(aluno);
		
		request.setAttribute("dadosAluno", dadosAluno);
		request.setAttribute("termoAditivo", termoAditivo);
		request.getRequestDispatcher("/form.jsp").forward(request, response);*/
	}

}
