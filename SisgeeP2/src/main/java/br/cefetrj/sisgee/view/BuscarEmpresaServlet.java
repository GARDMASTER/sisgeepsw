package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.AlunoServices;
import br.cefetrj.sisgee.control.EmpresaServices;
import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Convenio;
import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Servlet para buscar a entidade Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class BuscarEmpresaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static Empresa empresa;
	private static Convenio convenio;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cnpj = request.getParameter("cnpj");
		String numeroConvenio = request.getParameter("numeroConvenio");
		System.out.println(numeroConvenio);


		Aluno aluno = BuscarAlunoServlet.getAluno();
			List<Aluno> dadosAluno = new ArrayList<Aluno>();
			if (aluno!=null) {
				dadosAluno.add(aluno);
			}
		
		Empresa empresa = EmpresaServices.buscarEmpresaPorCnpj(cnpj);
			List<Empresa> dadosEmpresa = new ArrayList<Empresa>();
			dadosEmpresa.add(empresa);
			setEmpresa(empresa);
		
		Convenio convenio = EmpresaServices.buscarConvenio(numeroConvenio);
		System.out.println(convenio);
			if (convenio == null) {
				Convenio convenioIncluir = new Convenio(numeroConvenio, null, null, empresa);
				EmpresaServices.incluirConvenio(convenioIncluir);
				List<Convenio> dadosConvenio = new ArrayList<Convenio>();
				dadosConvenio.add(convenioIncluir);
				request.setAttribute("dadosConvenio", dadosConvenio);
				setConvenio(convenioIncluir);
			}else{
				List<Convenio> dadosConvenio = new ArrayList<Convenio>();
				dadosConvenio.add(convenio);
				request.setAttribute("dadosConvenio", dadosConvenio);
				setConvenio(convenio);
			}
		
		Empresa empresaAgente = BuscarEmpresaAgenteServlet.getEmpresaAgente();
		List<Empresa> dadosEmpresaAgente = new ArrayList<Empresa>();
		if (empresa!=null) {
			dadosEmpresa.add(empresaAgente);
		}
		
		request.setAttribute("dadosEmpresaAgente", dadosEmpresaAgente);
		request.setAttribute("dadosAluno", dadosAluno);
		request.setAttribute("dadosEmpresa", dadosEmpresa);
		request.getRequestDispatcher("/form.jsp").forward(request, response);
	}

	public static Empresa getEmpresa() {
		return empresa;
	}

	public static void setEmpresa(Empresa empresa) {
		BuscarEmpresaServlet.empresa = empresa;
	}

	public static Convenio getConvenio() {
		return convenio;
	}

	public static void setConvenio(Convenio convenio) {
		BuscarEmpresaServlet.convenio = convenio;
	}
	
	
	
}

