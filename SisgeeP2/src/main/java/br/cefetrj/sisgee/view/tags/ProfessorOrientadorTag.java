package br.cefetrj.sisgee.view.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.entity.ProfessorOrientador;

/**
 * Tag customizada da entidade ProfessorOrientador
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class ProfessorOrientadorTag extends SimpleTagSupport{
	
	private long id;
	
	@Override
	public void doTag() throws JspException, IOException {
		String retorno = ""
				
				+ "<label for='professorOrientador'> Professor Orientador: </label>"
				+ "<select id='professorOrientador' class='form-control' name='professorOrientador' value='${ param.professorOrientador }'>"
				+ "	<option value=''>...</option>";
		
		GenericDAO<ProfessorOrientador> professorOrientadorDAO = PersistenceManager.createGenericDAO(ProfessorOrientador.class);
		List<ProfessorOrientador> listaPO = professorOrientadorDAO.buscarTodos();
		
		for (ProfessorOrientador professorOrientador : listaPO) {
			retorno += "<option value='" + professorOrientador.getIdProfessorOrientador() +"' ";
			if(professorOrientador.getIdProfessorOrientador().equals(id)){
				retorno += " selected ";
			}
			retorno += " >" + professorOrientador.getNomeProfessorOrientador() + "</option>";
		}
		retorno += "</select></div>";
		
		getJspContext().getOut().append(retorno);
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
