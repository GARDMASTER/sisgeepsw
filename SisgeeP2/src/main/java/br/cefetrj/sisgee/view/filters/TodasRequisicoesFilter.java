package br.cefetrj.sisgee.view.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;

/**
 * Servlet Filter implementation class TodasRequisicoesFilter
 */
@WebFilter("/*")
public class TodasRequisicoesFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		Logger lg = Logger.getLogger(TodasRequisicoesFilter.class);
		lg.info("Requisições feitas para o path: " + request.getServletContext().getContextPath());
		try{
			chain.doFilter(request, response);
		} catch(Exception e){
			request.getRequestDispatcher("/erro.jsp").forward(request, response);
		}
	}


	public void init(FilterConfig fConfig) throws ServletException {
	}
	
	public void destroy() {
	}

}
