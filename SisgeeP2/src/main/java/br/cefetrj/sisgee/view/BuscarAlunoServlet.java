package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.AlunoServices;
import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Convenio;
import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Servlet para buscar a entidade Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class BuscarAlunoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Aluno aluno;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String matricula = request.getParameter("matricula");
		
		Aluno aluno = AlunoServices.buscarAlunoPorMatricula(matricula);
		List<Aluno> dadosAluno = new ArrayList<Aluno>();
		dadosAluno.add(aluno);
		
		Empresa empresa = BuscarEmpresaServlet.getEmpresa();
		List<Empresa> dadosEmpresa = new ArrayList<Empresa>();
		if (empresa!=null) {
			dadosEmpresa.add(empresa);
		}
		
		Empresa empresaAgente = BuscarEmpresaAgenteServlet.getEmpresaAgente();
		List<Empresa> dadosEmpresaAgente = new ArrayList<Empresa>();
		if (empresa!=null) {
			dadosEmpresa.add(empresaAgente);
		}
		
		Convenio convenio = BuscarEmpresaServlet.getConvenio();
		List<Convenio> dadosConvenio = new ArrayList<Convenio>();
		dadosConvenio.add(convenio);
		
		setAluno(aluno);
		
		request.setAttribute("dadosEmpresaAgente", dadosEmpresaAgente);
		request.setAttribute("dadosConvenio", dadosConvenio);
		request.setAttribute("dadosEmpresa", dadosEmpresa);
		request.setAttribute("dadosAluno", dadosAluno);
		
		String action = request.getParameter("action");
		request.getRequestDispatcher(action).forward(request, response);
	}

	public static Aluno getAluno() {
		return aluno;
	}

	public static void setAluno(Aluno aluno) {
		BuscarAlunoServlet.aluno = aluno;
	}
	

}