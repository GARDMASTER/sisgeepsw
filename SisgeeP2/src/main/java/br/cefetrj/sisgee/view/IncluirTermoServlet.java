package br.cefetrj.sisgee.view;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.ProfessorOrientadorServices;
import br.cefetrj.sisgee.control.TermoServices;
import br.cefetrj.sisgee.model.entity.Aluno;
import br.cefetrj.sisgee.model.entity.Convenio;
import br.cefetrj.sisgee.model.entity.ProfessorOrientador;
import br.cefetrj.sisgee.model.entity.TermoEstagio;

/**
 * Servlet para incluir a entidade TermoEstagio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class IncluirTermoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Aluno aluno = BuscarAlunoServlet.getAluno();
		Convenio convenio = BuscarEmpresaServlet.getConvenio();
		
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dataI = request.getParameter("dataInicio");
		String dataT = request.getParameter("dataTermino");
		
			Date dataInicio = null;
			try {
				dataInicio = new Date(format.parse(dataI).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			Date dataTermino = null;
			try {
				dataTermino = new Date(format.parse(dataT).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		String horasDia = request.getParameter("cargaHoraria");
			
			Short cargaHoraria = null;
			try {
				cargaHoraria = Short.parseShort(horasDia);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		String bolsa = request.getParameter("valorBolsa");
			
			Float valorBolsa = null;
			try {
				valorBolsa = Float.parseFloat(bolsa);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		String enderecoEstagio = request.getParameter("enderecoEstagio");
		String complementoEndereco = request.getParameter("complementoEndereco");
		String bairro = request.getParameter("bairro");
		String cidade = request.getParameter("cidade");
		String estado = request.getParameter("estado");
		String cep = request.getParameter("cep");
		String radioEstagio = request.getParameter("radioEstagio");
		String professorOrientador = request.getParameter("professorOrientador");
		
		ProfessorOrientador pOrientador = null;
		if(!(professorOrientador.equals(""))) {
			pOrientador = ProfessorOrientadorServices.buscarProfessorOrientadorPeloId(professorOrientador);
		}
		
		
		TermoEstagio termoEstagio = new TermoEstagio();
		termoEstagio.setBairroEnderecoTermoEstagio(bairro);
		termoEstagio.setCepEnderecoTermoEstagio(cep);
		termoEstagio.setCidadeEnderecoTermoEstagio(cidade);
		termoEstagio.setComplementoEnderecoTermoEstagio(complementoEndereco);
		termoEstagio.setEnderecoTermoEstagio(enderecoEstagio);
		termoEstagio.setEstadoEnderecoTermoEstagio(estado);
		termoEstagio.setDataInicioTermoEstagio(dataInicio);
		termoEstagio.setDataFimTermoEstagio(dataTermino);
		termoEstagio.setCargaHorariaTermoEstagio(cargaHoraria);
		termoEstagio.setValorBolsa(valorBolsa);
		termoEstagio.setAluno(aluno);
		termoEstagio.setConvenio(convenio);
		termoEstagio.setProfessorOrientador(pOrientador);
		
		if(radioEstagio.equals("S")) {
			termoEstagio.seteEstagioObrigatorio(1);
		}else {
			termoEstagio.seteEstagioObrigatorio(0);
		}
		
		TermoServices.incluirTermoEstagio(termoEstagio);
		
		request.getRequestDispatcher("/sucesso.jsp").forward(request, response);
		
	}

}
