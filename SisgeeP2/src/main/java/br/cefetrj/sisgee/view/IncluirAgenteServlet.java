package br.cefetrj.sisgee.view;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.cefetrj.sisgee.control.AgenteIntegracaoServices;
import br.cefetrj.sisgee.model.entity.AgenteIntegracao;

/**
 * Servlet para incluir a entidade AgenteIntegracao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class IncluirAgenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cnpjEmpresa = request.getParameter("cnpj");
		String nomeEmpresa = request.getParameter("razaoSocial");
		
		AgenteIntegracao AI = new AgenteIntegracao();
		AI.setCnpjAgenteIntegracao(cnpjEmpresa);
		AI.setNomeAgenteIntegracao(nomeEmpresa);
		AgenteIntegracaoServices.incluirAgenteIntegracao(AI);
		
		request.getRequestDispatcher("form.jsp").forward(request, response);
	}

}
