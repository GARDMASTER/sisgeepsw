package br.cefetrj.sisgee.control;


import java.util.ArrayList;
import java.util.List;

import br.cefetrj.sisgee.model.dao.AlunoDAO;
import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.entity.Aluno;

/**
 * Servi�os relacionados a classe Aluno
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */
public class AlunoServices {
	
	/**
	 * Busca todos Alunos
	 * 
	 * @return lista de aluno de todos alunos
	 */
	public static List<Aluno> listarAlunos(){
		GenericDAO<Aluno> alunoDao = PersistenceManager.createGenericDAO(Aluno.class);
		return alunoDao.buscarTodos();
	}
	
	/**
	 * Busca Aluno por Id
	 * 
	 * @return lista de aluno
	 */
	public static Aluno buscarAlunoPorId(Long id){
		GenericDAO<Aluno> alunoDao = PersistenceManager.createGenericDAO(Aluno.class);
		return alunoDao.buscar(id);
	}
	
	/**
	 * Busca Aluno por Matricula
	 * 
	 * @return objeto Aluno
	 */
	public static Aluno buscarAlunoPorMatricula(String matricula){
		AlunoDAO alunoDao = new AlunoDAO();
		return (Aluno) alunoDao.buscarAlunoPorString(matricula);
	}
}
