  package br.cefetrj.sisgee.control;

import java.util.List;

import br.cefetrj.sisgee.model.dao.ConvenioDAO;
import br.cefetrj.sisgee.model.dao.EmpresaDAO;
import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.entity.Convenio;
import br.cefetrj.sisgee.model.entity.Empresa;

/**
 * Servi�os relacionados a classe Empresa
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class EmpresaServices {
	
	/**
	 * Busca Empresa por CNPJ
	 * 
	 * @return objeto empresa
	 */
	public static Empresa buscarEmpresaPorCnpj(String cnpj){
		EmpresaDAO empresaDao = new EmpresaDAO();
		return empresaDao.buscarEmpresaPorString(cnpj);
	}
	
	/**
	 * Persiste a Empresa no banco de dados
	 * 
	 */
	public static void incluirEmpresa(Empresa empresa){
		GenericDAO<Empresa> empresaDao = PersistenceManager.createGenericDAO(Empresa.class);
		
		PersistenceManager.getTransaction().begin();
		try{
			empresaDao.incluir(empresa);
			PersistenceManager.getTransaction().commit();
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
		}
	}
	
	public static Convenio buscarConvenio(String numeroConvenio){
		ConvenioDAO convenioDao = new ConvenioDAO();
		return convenioDao.buscarConvenioPorString(numeroConvenio);
	}
	
	public static void incluirConvenio(Convenio convenio){
		GenericDAO<Convenio> convenioDao = PersistenceManager.createGenericDAO(Convenio.class);
		
		PersistenceManager.getTransaction().begin();
		try{
			convenioDao.incluir(convenio);
			PersistenceManager.getTransaction().commit();
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
		}
	}
	
	public static Empresa buscarEmpresaPorAgente(String cnpj, Long idAgente){
		EmpresaDAO empresaDao = new EmpresaDAO();
		return empresaDao.buscarEmpresaPorAgente(cnpj, idAgente);
	}
	
}

