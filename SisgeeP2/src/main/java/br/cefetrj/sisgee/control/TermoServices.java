package br.cefetrj.sisgee.control;

import java.sql.Date;

import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.dao.TermoDAO;
import br.cefetrj.sisgee.model.entity.TermoEstagio;

/**
 * Servi�os relacionados a classe TermoEstagio
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class TermoServices {
		
	
	/**
	 * Persiste o TermoEstagio no banco de dados
	 * 
	 */
	public static void incluirTermoEstagio(TermoEstagio termoEstagio){
		GenericDAO<TermoEstagio> termoE = PersistenceManager.createGenericDAO(TermoEstagio.class);
		
		PersistenceManager.getTransaction().begin();
		try{
			termoE.incluir(termoEstagio);
			
			PersistenceManager.getTransaction().commit();
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
		}
	}
	
	public static void incluirDataRescisao(Date dataRescisao, Long idaluno){
		TermoDAO termoDAO = new TermoDAO();
		PersistenceManager.getTransaction().begin();
		try{
			

			termoDAO.incluirDataRescisao(dataRescisao, idaluno);
			
			PersistenceManager.getTransaction().commit();
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
		}
		
	}
	
	public static TermoEstagio buscarTermoEstagioPorAluno(Long idaluno){
		TermoDAO termoDao = new TermoDAO();
		return termoDao.buscarTermoEstagioPorAluno(idaluno);
	}
	
	public static boolean alterarTermoEstagio(TermoEstagio termoEstagio){
		GenericDAO<TermoEstagio> termoE = PersistenceManager.createGenericDAO(TermoEstagio.class);
		
		PersistenceManager.getTransaction().begin();
		try{
			termoE.alterar(termoEstagio);
			PersistenceManager.getTransaction().commit();
			return true;
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
			return false;
		}
	}
	
}
