package br.cefetrj.sisgee.control;

import br.cefetrj.sisgee.model.dao.AgenteIntegracaoDAO;
import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.entity.AgenteIntegracao;

/**
 * Servi�os relacionados a classe AgenteIntegracao
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */
public class AgenteIntegracaoServices {
	
	/**
	 * Busca AgenteIntegracao por CNPJ
	 * 
	 * @return objeto AgenteIntegracao
	 */
	public static AgenteIntegracao buscarAgentePorCnpj(String cnpj){
		AgenteIntegracaoDAO agenteIntegracaoDAO = new AgenteIntegracaoDAO();
		return agenteIntegracaoDAO.buscarAgentePorString(cnpj);
	}
	
	
	
	
	
	/**
	 * Persiste o agenteIntegracao no banco de dados
	 * 
	 */
	public static void incluirAgenteIntegracao(AgenteIntegracao agenteIntegracao){
		GenericDAO<AgenteIntegracao> agenteIntegracaoDAO = PersistenceManager.createGenericDAO(AgenteIntegracao.class);
		
		PersistenceManager.getTransaction().begin();
		try{
			agenteIntegracaoDAO.incluir(agenteIntegracao);
			PersistenceManager.getTransaction().commit();
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
		}
	}
}
