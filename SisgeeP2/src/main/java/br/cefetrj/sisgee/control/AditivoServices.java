package br.cefetrj.sisgee.control;

import br.cefetrj.sisgee.model.dao.GenericDAO;
import br.cefetrj.sisgee.model.dao.PersistenceManager;
import br.cefetrj.sisgee.model.entity.TermoAditivo;


/**
 * Servi�os relacionados a classe TermoAditivo
 * 
 * @author Daniel Guinin, Giovanni Duarte, Francisco Chernicharo, Michael Wallace
 * @since 1.0
 */

public class AditivoServices {
	
	/**
	 * Recupera o termoAditivo pelo Aluno
	 * 
	 * @return lista de aditivos de certo aluno
	 */
	
	/*public static List buscarAditivoPorAluno(Aluno aluno){
		AditivoDAO aditivoDao = new AditivoDAO();
		return aditivoDao.buscarAditivosPorAluno(aluno);
		
	}*/
	
	/**
	 * Persiste o TermoAditivo no banco de dados
	 * 
	 * 
	 */
	
	public static void incluirTermoAditivo(TermoAditivo termoAditivo){
		GenericDAO<TermoAditivo> termoAditivoDao = PersistenceManager.createGenericDAO(TermoAditivo.class);
		
		PersistenceManager.getTransaction().begin();
		try{
			termoAditivoDao.incluir(termoAditivo);
			PersistenceManager.getTransaction().commit();
		}catch(Exception e){
			PersistenceManager.getTransaction().rollback();
		}
	}
	
}
