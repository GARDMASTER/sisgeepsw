package br.cefetrj.sisgee.control;

import br.cefetrj.sisgee.model.dao.ProfessorOrientadorDAO;
import br.cefetrj.sisgee.model.entity.ProfessorOrientador;

public class ProfessorOrientadorServices {
	/**
	 * Busca ProfessorOrientador por ID
	 * 
	 * @return objeto ProfessorOrientador
	 */
	public static ProfessorOrientador buscarProfessorOrientadorPeloId(String id){
		ProfessorOrientadorDAO professorOrientadorDAO = new ProfessorOrientadorDAO();
		return (ProfessorOrientador) professorOrientadorDAO.buscarProfessorOrientadorPorId(Long.parseLong(id));
	}
}
