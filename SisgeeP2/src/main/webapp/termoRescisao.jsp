<!doctype html>
<html lang="pt-br">
<head>
<%@ include file="head_imports.jspf"%>
<title><fmt:message key="br.cefetrj.sisgee.titulo_sistema"></fmt:message></title>
<%@ include file="script_radio.jspf" %>
</head>

<body class="bg-light">

	<%@ include file="menu.jspf"%>
	<%@ include file="carrosel.jspf"%>
	<%@ include file="sidebar.jspf"%>

	<!-- Conte�do da P�gina -->
	<div class="col-md-9">
		<div class="container">
			<p class="h5 form-row font-weight-bold mr-md-2"><fmt:message key="br.cefetrj.sisgee.sidebar.registro_recis�o"/></p>
			<hr>

			<form method="POST" action="BuscarAlunoServlet?action=termoRescisao.jsp">
				<c:if test="${ empty dadosAluno }">

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.matricula"/></label>
							<div class="form-inline" id="inputEmail4">
								<input class="form-control mr-sm-2" type="search"
									placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message>" aria-label="Search" name="matricula"
									value="${ param.matricula }" />
								<button class="btn btn-outline-secondary my-2 my-sm-0"
									type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
							</div>
						</div>
						<div class="form-group col-md-6">
							<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.nome"></fmt:message></label> <input type="text"
								class="form-control" id="inputPassword4" name="nome"
								value="${ param.nome }" />
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.curso"></fmt:message></label> <input type="text"
								class="form-control" id="inputPassword4" name="curso"
								value="${ param.curso }" />
						</div>
						<div class="form-group col-md-8">
							<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.unidade"></fmt:message></label> <input type="text"
								class="form-control" id="inputPassword4" name="unidade"
								value="${ param.unidade }" />
						</div>
					</div>

				</c:if>
				<c:if test="${ not empty dadosAluno }">
					<c:forEach items="${ dadosAluno }" var="aluno">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.matricula"/></label>
								<div class="form-inline" id="inputEmail4">
									<input class="form-control mr-sm-2" type="search"
										placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message>" aria-label="Search" name="matricula"
										value="${ aluno.matricula }" />
									<button class="btn btn-outline-secondary my-2 my-sm-0"
										type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.nome"></fmt:message></label> <input type="text"
									class="form-control" id="inputPassword4" name="nome" value="${ aluno.getPessoa().getNome() }" />
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.curso"/></label> <input type="text"
									class="form-control" id="inputPassword4" name="curso" value="${ aluno.getCurso().getNomeCurso() }" />
							</div>
							<div class="form-group col-md-8">
								<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.unidade"/></label> <input type="text"
									class="form-control" id="inputPassword4" name="unidade"
									value="${ aluno.getCurso().getCampus().getNomeCampus() }" />
							</div>
						</div>

					</c:forEach>
				</c:if>
			</form>
	
			<form method="POST" action="IncluirTermoRescisaoServlet">
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputCity"><fmt:message key="br.cefetrj.sisgee.termo_recis�o.data_recisao"/></label> <input type="date"
							class="form-control" id="inputCity" placeholder="  /  /  " name="dataRescisao"
									value="${ param.dataRescisao }" />
					</div>
				</div>
	
				<hr>
				<div class="form-inline my-2 my-lg-0">
					<button type="submit" class="btn btn-primary btn-md mr-sm-2"><fmt:message key="br.cefetrj.sisgee.salvar" /></button>
					<button type="button" onclick="javascript:location.href='index.jsp'" class="btn btn-secondary btn-md"><fmt:message key="br.cefetrj.sisgee.cancelar" /></button>
				</div>
			</form>

		
	</div>
	</div>
	<!-- Fim conte�do da p�gina -->
	</div>
	</div>
	</main>
	<!-- Fim main -->

	<%@ include file="footer.jspf"%>
	<%@ include file="scripts_imports.jspf"%>

</body>
</html>
