<!doctype html>
<html lang="pt-br">
<head>
	<%@ include file="head_imports.jspf" %>
	<title><fmt:message key="br.cefetrj.sisgee.titulo_sistema"/></title>
</head>

<body class="bg-light">
	
	<%@ include file="menu.jspf" %>
	<%@ include file="carrosel.jspf" %>
	<%@ include file="sidebar.jspf" %>

				<!-- Conte�do da P�gina -->
				
				<div class="col-md-9">
					<div class="container">
						<p class="h4"><fmt:message key="br.cefetrj.sisgee.index.pagina_descricao"/></p>
					</div>
				</div>
				<!-- Fim conte�do da p�gina -->
			</div>
		</div>
	</main><!-- Fim main -->
	
	<%@ include file="footer.jspf" %>
	
	<%@ include file="scripts_imports.jspf" %>

</body>
</html>
