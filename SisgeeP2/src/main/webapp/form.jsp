<%@ taglib prefix="cmp" uri="WEB-INF/components.tld"%>
<!doctype html>
<html lang="pt-br">
<head>
<%@ include file="head_imports.jspf"%>

<title><fmt:message key="br.cefetrj.sisgee.titulo_sistema" /></title>
<%@ include file="script_radio.jspf" %>
</head>

<body class="bg-light">

	<%@ include file="menu.jspf"%>
	<%@ include file="carrosel.jspf"%>
	<%@ include file="sidebar.jspf"%>

	<!-- Conte�do da P�gina -->
	<div class="col-md-9">
	<div class="container">
		<c:if test="${ not empty msg }">
			<p class="h5">${ msg }</p>
		</c:if>
		<form action="" method="POST">
			<p class="h5 form-row font-weight-bold mr-md-2"><fmt:message key="br.cefetrj.sisgee.form.dados_empresa"></fmt:message></p>
			<hr>

			<form id="options" class="form-inline">
				<label class="mr-sm-2" for="inlineFormCustomSelectPref"><fmt:message key="br.cefetrj.sisgee.form.agente_integrador"/></label>

				<div class="form-check form-check-inline">
					<label class="form-check-label"> <input
						class="form-check-input" onclick="javascript:optionCheck();"
						type="radio" name="radioAgente" id="inlineRadio1" value="S"
						${ param.radio eq "S" ? "checked" : ""}> <fmt:message key="br.cefetrj.sisgee.form.sim"></fmt:message>
					</label>
				</div>
				<div class="form-check form-check-inline">
					<label class="form-check-label"> <input
						class="form-check-input" onclick="javascript:optionCheck();"
						type="radio" name="radioAgente" id="inlineRadio2" value="N"
						${ param.radio eq "N" ? "checked" : ""}> <fmt:message key="br.cefetrj.sisgee.form.nao"></fmt:message>
					</label>
				</div>

			</form>



			<form method="POST" action="BuscarEmpresaServlet">
				
				<div class="form-group">
					<label for="inputAddress">N�mero do Conv�nio</label>
					<c:if test="${ empty dadosConvenio }"> 
					<input type="text" class="form-control" id="inputAddress" name="numeroConvenio" value="${ param.numeroConvenio }">
					</c:if>
					<c:if test="${ not empty dadosConvenio }">
					<c:forEach items="${ dadosConvenio }" var="convenio">
						<input type="text" class="form-control" id="inputAddress" name="numeroConvenio" value="${ convenio.numeroConvenio }">
					</c:forEach>
					</c:if>
				</div>

				<div id="hiddenNao">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputEmail4">CNPJ</label>
							<div class="form-inline" id="inputEmail4">
								<c:if test="${ empty dadosEmpresa }">
									<input class="form-control mr-sm-2" type="search"
										placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"/>" aria-label="Search" name="cnpj"
										value="${ param.cnpj }">
								</c:if>
								<c:if test="${ not empty dadosEmpresa }">
									<c:forEach items="${ dadosEmpresa }" var="empresa">
										<input class="form-control mr-sm-2" type="search"
											placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message>" aria-label="Search" name="cnpj"
											value="${ empresa.cnpjEmpresa }">
									</c:forEach>
								</c:if>
								<button class="btn btn-outline-secondary my-2 my-sm-0"
									type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-8">
							<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.razaosocial"/></label>
							<div class="form-inline" id="inputEmail4">
								<c:if test="${ empty dadosEmpresa }">
									<input class="form-control col-md-8 mr-sm-2" type="text"
										class="form-control" id="inputPassword4" name="razaoSocial"
										value="${ param.razaoSocial }">
								</c:if>
								<c:if test="${ not empty dadosEmpresa }">
									<c:forEach items="${ dadosEmpresa }" var="empresa">
										<input class="form-control col-md-8 mr-sm-2" type="text"
											class="form-control" id="inputPassword4" name="razaoSocial"
											value="${ empresa.nomeEmpresa }">
									</c:forEach>
								</c:if>
								<button class="btn btn-outline-light my-2 my-sm-0"
									style="background: transparent;" type="button"
									onclick="javascript:popUpEmpresa();">
									<img alt="adicionar" src="images/plus.png">
								</button>
							</div>
						</div>
					</div>
				</div>
			</form>

			<div id="hiddenSim" style="display: none" class="form-row">
				<div class="form-row">
					<div class="form-group col-md-7">
						<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.razaosocial"/></label>
						<div class="form-group" id="inputEmail4">
							<cmp:ComboAgenteIntegracao id=""/>
						</div>
					</div>
					<div class="form-group col-md-1">
						<div class="form-group" id="inputEmail4">
						<label style="color:transparent" for="inputEmail4">.</label>
							<button class="btn btn-outline-light my-2 my-sm-0"
								style="background: transparent;" type="button"
								onclick="javascript:popUpEmpresa();">
								<img alt="adicionar" src="images/plus.png" />
							</button>
						</div>
					</div>

				</div>
				<div class="form-group col-md-6">
					<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.CNPJ_empresa_ligada"/></label>
					<form class="form-inline" id="inputEmail4">
						<input class="form-control mr-sm-2" type="search"
							placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message>" aria-label="Search">
						<button class="btn btn-outline-secondary my-2 my-sm-0"
							type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
					</form>
				</div>
				<div class="form-row">
					<div class="form-group col-md-8">
						<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.razao_social_empresa_ligada"/></label>
						<form class="form-inline" id="inputEmail4">
							<input class="form-control col-md-8 mr-sm-2" type="text"
								class="form-control" id="inputPassword4">
							<button class="btn btn-outline-light my-2 my-sm-0"
								style="background: transparent;" type="button"
								onclick="javascript:popUpAgente();">
								<img alt="adicionar" src="images/plus.png"/ >
							</button>
						</form>
					</div>

				</div>
			</div>

			<hr>
			<p class="h5 form-row font-weight-bold"><fmt:message key="br.cefetrj.sisgee.form.dados_aluno"></fmt:message></p>
			<hr>




			<form method="POST" action="BuscarAlunoServlet?action=form.jsp">
				<c:if test="${ empty dadosAluno }">

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.matricula"/></label>
							<div class="form-inline" id="inputEmail4">
								<input class="form-control mr-sm-2" type="search"
									placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message>" aria-label="Search" name="matricula"
									value="${ param.matricula }" />
								<button class="btn btn-outline-secondary my-2 my-sm-0"
									type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
							</div>
						</div>
						<div class="form-group col-md-6">
							<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.nome"></fmt:message></label> <input type="text"
								class="form-control" id="inputPassword4" name="nome"
								value="${ param.nome }" />
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.curso"></fmt:message></label> <input type="text"
								class="form-control" id="inputPassword4" name="curso"
								value="${ param.curso }" />
						</div>
						<div class="form-group col-md-8">
							<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.unidade"></fmt:message></label> <input type="text"
								class="form-control" id="inputPassword4" name="unidade"
								value="${ param.unidade }" />
						</div>
					</div>

				</c:if>
				<c:if test="${ not empty dadosAluno }">
					<c:forEach items="${ dadosAluno }" var="aluno">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputEmail4"><fmt:message key="br.cefetrj.sisgee.form.matricula"/></label>
								<div class="form-inline" id="inputEmail4">
									<input class="form-control mr-sm-2" type="search"
										placeholder="<fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message>" aria-label="Search" name="matricula"
										value="${ aluno.matricula }" />
									<button class="btn btn-outline-secondary my-2 my-sm-0"
										type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.nome"></fmt:message></label> <input type="text"
									class="form-control" id="inputPassword4" name="nome" value="${ aluno.getPessoa().getNome() }" />
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.curso"/></label> <input type="text"
									class="form-control" id="inputPassword4" name="curso" value="${ aluno.getCurso().getNomeCurso() }" />
							</div>
							<div class="form-group col-md-8">
								<label for="inputPassword4"><fmt:message key="br.cefetrj.sisgee.form.unidade"/></label> <input type="text"
									class="form-control" id="inputPassword4" name="unidade"
									value="${ aluno.getCurso().getCampus().getNomeCampus() }" />
							</div>
						</div>

					</c:forEach>
				</c:if>
			</form>






			<form method="POST" action="IncluirTermoServlet">
				<hr>
				<p class="h5 form-row font-weight-bold"><fmt:message key="br.cefetrj.sisgee.form.vigencia_estagio"></fmt:message></p>
				<hr>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputCity"><fmt:message key="br.cefetrj.sisgee.form.data_de_inicio"></fmt:message></label> <input type="date"
							class="form-control" id="inputCity" placeholder="  /  /  "
							name="dataInicio" value="${ param.dataInicio }" />
					</div>
					<div class="form-group col-md-6">
						<label for="inputState"><fmt:message key="br.cefetrj.sisgee.form.data_de_termino"></fmt:message></label> <input type="date"
							class="form-control" id="inputCity" placeholder="  /  /  "
							name="dataTermino" value="${ param.dataTermino }" />
					</div>
				</div>

				<hr>
				<p class="h5 form-row font-weight-bold"><fmt:message key="br.cefetrj.sisgee.form.carga_horaria_aluno"/></p>
				<hr>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="inputCity"><fmt:message key="br.cefetrj.sisgee.form.horas_por_dia"></fmt:message></label> <input type="text"
							class="form-control" id="inputCity" placeholder="horas"
							name="cargaHoraria" value="${ param.cargaHoraria }" />
					</div>
					<div class="form-group col-md-6"></div>
				</div>

				<hr>
				<p class="h5 form-row font-weight-bold"><fmt:message key="br.cefetrj.sisgee.form.valor_bolsa"></fmt:message></p>
				<hr>
				<div class="form-row">
					<div class="form-group col-md-4">
						<label for="inputAddress2"><fmt:message key="br.cefetrj.sisgee.form.valor"></fmt:message></label> <input type="text"
							class="form-control" id="inputAddress2" name="valorBolsa"
							value="${ param.valorBolsa }" />
					</div>
				</div>

				<hr>
				<p class="h5 form-row font-weight-bold"><fmt:message key="br.cefetrj.sisgee.form.local_do_estagio"/></p>
				<hr>
				<div class="form-row">
					<div class="form-group col-md-7">
						<label for="inputAddress2"><fmt:message key="br.cefetrj.sisgee.form.endereco"></fmt:message></label> <input type="text"
							class="form-control" id="inputAddress2" name="enderecoEstagio"
							value="${ param.enderecoEstagio }" />
					</div>
					<div class="form-group col-md-5">
						<label for="inputAddress2"><fmt:message key="br.cefetrj.sisgee.form.complemento"></fmt:message></label> <input type="text"
							class="form-control" id="inputAddress2"
							name="complementoEndereco" value="${ param.complementoEndereco }" />
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-3">
						<label for="inputCity"><fmt:message key="br.cefetrj.sisgee.form.bairro"></fmt:message></label> <input type="text"
							class="form-control" id="inputCity" name="bairro"
							value="${ param.bairro }" />
					</div>
					<div class="form-group col-md-3">
						<label for="inputCity"><fmt:message key="br.cefetrj.sisgee.form.cidade"></fmt:message></label> <input type="text"
							class="form-control" id="inputCity" name="cidade"
							value="${ param.cidade }" />
					</div>
					<div class="form-group col-md-4">
						<label for="inputState"><fmt:message key="br.cefetrj.sisgee.form.estado"/></label> <select id="inputState"
							class="form-control" name="estado" value="${ param.estado }">
							<option value="" selected><fmt:message key="br.cefetrj.sisgee.form.selecione"></fmt:message></option>
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amap�</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Cear�</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Esp�rito Santo</option>
							<option value="GO">Goi�s</option>
							<option value="MA">Maranh�o</option>
							<option value="MT">Mato Grosso</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Par�</option>
							<option value="PB">Para�ba</option>
							<option value="PR">Paran�</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piau�</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rond�nia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">S�o Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>
						</select>
					</div>
					<div class="form-group col-md-2">
						<label for="inputZip">CEP</label> <input type="text"
							class="form-control" id="inputZip" name="cep"
							value="${ param.cep }" />
					</div>
				</div>

				<div class="form-group form-inline">
					<label class="mr-sm-2" for="inlineFormCustomSelectPref"><fmt:message key="br.cefetrj.sisgee.form.estadio_obrigatorio_question"/></label>

					<div class="form-check form-check-inline">
						<label class="form-check-label"> <input
							class="form-check-input" type="radio" name="radioEstagio"
							id="inlineRadio1" value="S"
							${ param.radioEstagio eq "S" ? "checked" : ""}> <fmt:message key="br.cefetrj.sisgee.form.sim"></fmt:message>
						</label>
					</div>
					<div class="form-check form-check-inline">
						<label class="form-check-label"> <input
							class="form-check-input" type="radio" name="radioEstagio"
							id="inlineRadio2" value="N"
							${ param.radioEstagio eq "N" ? "checked" : ""}> <fmt:message key="br.cefetrj.sisgee.form.nao"></fmt:message>
						</label>
					</div>

				</div>


				<div class="form-row">
					<div class="form-group col-md-4">
						<cmp:ComboSistema id="" />
					</div>


					<hr>
					<br/>

					<div class="form-inline my-2 my-lg-0">
						<button type="submit" class="btn btn-primary btn-md mr-sm-2"><fmt:message key="br.cefetrj.sisgee.salvar"/></button>
						<button type="button" onclick="javascript:location.href='index.jsp'" class="btn btn-secondary btn-md"><fmt:message key="br.cefetrj.sisgee.cancelar"/></button>
					</div>
				</div>
			</form>



		</form>
	</div>
	</div>
	<!-- Fim conte�do da p�gina -->
	</div>
	</div>
	</main>
	<!-- Fim main -->

	<%@ include file="footer.jspf"%>
	<%@ include file="scripts_imports.jspf"%>

</body>
</html>