<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="pt-br">
<head>
<%@ include file="head_imports.jspf"%>
<title><fmt:message key="br.cefetrj.sisgee.titulo_sistema"></fmt:message></title>
</head>

<body class="bg-light">

	<%@ include file="menu.jspf"%>
	<%@ include file="carrosel.jspf"%>
	<%@ include file="sidebar.jspf"%>

	<!-- Conteúdo da Página -->
	<div class="col-md-9">
	<div class="container">
		<c:if test="${ not empty msg }">
			<p class="h5">${ msg }</p>
		</c:if>
		<form action="ValidaEmpresaServlet" method="POST">
			<p class="h5 form-row font-weight-bold mr-md-2">
				<fmt:message key="br.cefetrj.sisgee.registroEmpresa.registro" />
			</p>
			<hr>

			<div class="form-inline">
				<label class="mr-sm-2" for="inlineFormCustomSelectPref"><fmt:message
						key="br.cefetrj.sisgee.registroEmpresa.agente_integração" /></label>

				<div class="form-check form-check-inline">
					<label class="form-check-label"> <input
						class="form-check-input" type="radio" name="radio"
						id="inlineRadio1" value="S"
						${ param.radio eq "S" ? "checked" : ""} /> <fmt:message
							key="br.cefetrj.sisgee.sim"></fmt:message>
					</label>
				</div>
				<div class="form-check form-check-inline">
					<label class="form-check-label"> <input
						class="form-check-input" type="radio" name="radio"
						id="inlineRadio2" value="N"
						${ param.radio eq "N" ? "checked" : ""} /> <fmt:message
							key="br.cefetrj.sisgee.nao"></fmt:message>
					</label>
				</div>
			</div>
			<div class="form-group">
				<label for="inputAddress">CNPJ</label> <input type="text"
					class="form-control" name="cnpj" id="inputAddress"
					value="${ param.cnpj }" />
			</div>
			<div class="form-group">

				<label for="inputAddress"><fmt:message key="br.cefetrj.sisgee.razaosocial"></fmt:message></label> <input type="text"
					class="form-control" name="razaoSocial" id="inputAddress"
					value="${ param.razaoSocial }" />
			</div>

			</br>

			<div class="form-inline my-2 my-lg-0">
				<button type="submit" class="btn btn-primary btn-md mr-sm-2"><fmt:message key="br.cefetrj.sisgee.salvar"></fmt:message></button>
				<button type="button" onclick="javascript:location.href='form.jsp'" class="btn btn-secondary btn-md"><fmt:message key="br.cefetrj.sisgee.cancelar"></fmt:message></button>
			</div>
			</br>
		</form>
	</div>
	</div>
	<!-- Fim conteúdo da página -->
	</div>
	</div>
	</main>
	<!-- Fim main -->

	<%@ include file="footer.jspf"%>

	<%@ include file="scripts_imports.jspf"%>

</body>
</html>
