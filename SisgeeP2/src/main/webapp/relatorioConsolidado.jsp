<!doctype html>
<html lang="pt-br">
<head>
<%@ include file="head_imports.jspf"%>
<title><fmt:message key="br.cefetrj.sisgee.titulo_sistema"></fmt:message></title>
</head>

<body class="bg-light">

	<%@ include file="menu.jspf"%>
	<%@ include file="carrosel.jspf"%>
	<%@ include file="sidebar.jspf"%>

	<!-- Conte�do da P�gina -->
	<div class="col-md-9">
		<div class="container">
		<form>
			<p class="h5 form-row font-weight-bold mr-md-2"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.pesquisa_relat�rio"></fmt:message></p>
			<hr>


			<p class="h6 form-row font-weight-bold"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.vigencia_estagio"></fmt:message></p>
			<hr>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputCity"><fmt:message key="br.cefetrj.sisgee.data_de_inicio"></fmt:message></label> <input type="date"
						class="form-control" id="inputCity" placeholder="  /  /  ">
				</div>
				<div class="form-group col-md-6">
					<label for="inputState"><fmt:message key="br.cefetrj.sisgee.data_de_termino"></fmt:message></label> <input type="date"
						class="form-control" id="inputCity" placeholder="  /  /  ">
				</div>
			</div>
		
			<div class="d-flex justify-content-center">
			<form class="form-inline my-2 my-lg-0">
				<button class="btn btn-outline-secondary my-2 my-sm-0" type="submit"><fmt:message key="br.cefetrj.sisgee.buscar"></fmt:message></button>
			</form>
			</div>
		</form>
		
		<hr>
		<!--<p class="h6 form-row mr-md-2 text-center">Resultados da pesquisa</p>-->

		<div class="card">
			<div class="card-header font-weight-bold h6">T�cnico em inform�tica</div>
			<div class="card-body">
				
					<table class="table table-sm">
						<thead>
							<tr>
								<th scope="col"></th>
								<th scope="col" class="font-weight-bold h6">Total</th>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								
								<td scope="row" class="h6"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.termos_estagio_registrados"/></td>
								<td class="h6">250</td>
								
							</tr>
							<tr>
								
								<td scope="row" class="h6"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.aditivos_registrados" /></td>
								<td class="h6">110</td>
								
							</tr>
							<tr>
								
								<td scope="row" class="h6"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.recisoes_registradas" /></td>
								<td class="h6">200</td>
								
							</tr>
						</tbody>
					</table>
					
				
			</div>
		</div>
		
		</br>
		
		<div class="card">
			<div class="card-header font-weight-bold h6">Engenharia El�trica</div>
			<div class="card-body">
				
					<table class="table table-sm">
						<thead>
							<tr>
								<th scope="col"></th>
								<th scope="col" class="font-weight-bold h6">Total</th>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								
								<td scope="row" class="h6"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.termos_estagio_registrados"/></td>
								<td class="h6">250</td>
								
							</tr>
							<tr>
								
								<td scope="row" class="h6"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.aditivos_registrados" /></td>
								<td class="h6">110</td>
								
							</tr>
							<tr>
								
								<td scope="row" class="h6"><fmt:message key="br.cefetrj.sisgee.relatorio_consolidado.recisoes_registradas" /></td>
								<td class="h6">200</td>
								
							</tr>
						</tbody>
					</table>
					
				
			</div>
		</div>




	</div>
	</div>
	<!-- Fim conte�do da p�gina -->
	</div>
	</div>
	</main>
	<!-- Fim main -->

	<%@ include file="footer.jspf"%>

	<%@ include file="scripts_imports.jspf"%>

</body>
</html>
