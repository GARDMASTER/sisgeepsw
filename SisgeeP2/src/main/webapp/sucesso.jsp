<!doctype html>
<html lang="pt-br">
<head>
<%@ include file="head_imports.jspf"%>
<title>Sistema de Gest�o de Est�gio e Emprego | CEFET-RJ</title>
</head>

<body class="bg-light">

	<%@ include file="menu.jspf"%>
	<%@ include file="carrosel.jspf"%>
	<%@ include file="sidebar.jspf"%>

	<!-- Conte�do da P�gina -->
	
	<div class="col-md-9">
	<div class="container">
		<p class="h5">Formul�rio cadastrado com sucesso.</p>
	</div>
	</div>

	<!-- Fim conte�do da p�gina -->
	
	<!-- Fim main -->

	<%@ include file="footer.jspf"%>

	<%@ include file="scripts_imports.jspf"%>

</body>
</html>